#include <catch2/catch.hpp>
#include "graph.hpp"

TEST_CASE("happy pass Dijkstra")
{
    graph gr(8);
    gr.add(0, 1, 9.);
    gr.add(0, 4, 1.);
    gr.add(1, 0, 9.);
    gr.add(1, 2, 9.);
    gr.add(1, 5, 1.);
    gr.add(2, 1, 9.);
    gr.add(2, 3, 1.);
    gr.add(2, 6, 1.);
    gr.add(3, 2, 1.);
    gr.add(3, 7, 1.);
    gr.add(4, 0, 1.);
    gr.add(4, 5, 1.);
    gr.add(5, 1, 1.);
    gr.add(5, 4, 1.);
    gr.add(5, 6, 1.);
    gr.add(6, 2, 1.);
    gr.add(6, 5, 1.);
    gr.add(6, 7, 9.);
    gr.add(7, 3, 1.);
    gr.add(7, 6, 9.);
    /*          GRAPH
     *  0----(9)----1-----(9)-----2-----(1)----3
     *  |           |             |            |
     *  |           |             |            |
     * (1)         (1)           (1)          (1)
     *  |           |             |            |
     *  |           |             |            |
     *  4----(1)----5-----(1)-----6-----(9)----7
     */
    CHECK(gr.dijkstra(0, 7) == std::vector<unsigned> {7, 3, 2, 6, 5, 4, 0});
}
