#include <vector>

struct edge
{
    unsigned src;
    unsigned dest;
};

struct vertex
{
    unsigned value;
    double priority;

    vertex() = delete;
    vertex(unsigned v, double p) : value(v), priority(p) {}

    vertex& operator=(const vertex &other) = default;

    friend bool operator<(const vertex &lhs, const vertex &rhs)
    {
        return lhs.priority < rhs.priority;
    }
    friend bool operator>(const vertex &lhs, const vertex &rhs)
    {
        return lhs.priority > rhs.priority;
    }
};

class graph
{ 
    private:
        using weight = double;
        using adjacent_vector = std::vector<std::vector<weight>>;
        using result = std::vector<unsigned>;
        adjacent_vector table;
        const double INF;

        double cost(unsigned, unsigned) const;

    public:
        graph() = delete;
        explicit graph(unsigned);
        ~graph() = default;

        void add(unsigned, unsigned, double);
        result dijkstra(unsigned, unsigned);
};
