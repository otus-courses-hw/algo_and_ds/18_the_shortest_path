#include "graph.hpp"
#include <unordered_map>
#include <queue>
#include <limits>

graph::graph(unsigned vertices) :
    table(vertices),
    INF(std::numeric_limits<weight>::max())
{
    for (auto &row : table)
        row.resize(vertices, INF);
}

double graph::cost(unsigned a, unsigned b) const
{
    return table.at(a).at(b);
}

void graph::add(unsigned a, unsigned b, double w)
{
    table.at(a).at(b) = w;
}

auto graph::dijkstra(unsigned src, unsigned dest) -> result
{
    std::unordered_map<unsigned, unsigned> path;
    std::unordered_map<unsigned, double> aggregated_cost;
    aggregated_cost[src] = 0.;

    std::priority_queue<vertex, std::vector<vertex>, std::greater<>> q; // act like min priority queue
    q.emplace(src, aggregated_cost.at(src));

    while (!q.empty())
    {
        auto v = q.top();
        auto current = v.value;
        q.pop();

        if (current == dest)
            break;

        auto &row = table.at(current);
        for (std::size_t next = 0; next < row.size(); ++next)
        {
            if (auto weight = row.at(next); weight == INF)
                continue;
            
            auto new_cost = aggregated_cost[current] + this->cost(current, next);

            if (!aggregated_cost.contains(next) || new_cost < aggregated_cost[next])
            {
                aggregated_cost[next] = new_cost;
                path[next] = current;
                auto priority = new_cost;
                q.emplace(next, priority);
            }
        }
    }

    result r;
    for (auto vertex = dest; vertex != src;)
    {
        r.push_back(vertex);
        vertex = path.at(vertex);
    }
    r.push_back(src);

    return r;
}
